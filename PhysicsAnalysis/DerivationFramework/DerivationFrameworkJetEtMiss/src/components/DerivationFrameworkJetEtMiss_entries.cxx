#include "../JetDecorAlg.h"
#include "../JetAugmentationTool.h"
#include "../PFlowAugmentationTool.h"
#include "../METTriggerAugmentationTool.h"
#include "../ViewContainerThinning.h"
#include "../JetExternalAssocTool.h"

using namespace DerivationFramework;
 
DECLARE_COMPONENT( JetDecorAlg )
DECLARE_COMPONENT( JetAugmentationTool )
DECLARE_COMPONENT( PFlowAugmentationTool )
DECLARE_COMPONENT( METTriggerAugmentationTool )
DECLARE_COMPONENT( ViewContainerThinning )
DECLARE_COMPONENT( JetExternalAssocTool )

