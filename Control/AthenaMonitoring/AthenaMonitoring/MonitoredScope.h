/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef AthenaMonitoring_MonitoredScope_h
#define AthenaMonitoring_MonitoredScope_h

#warning "This is an obsolete header. Use MonitoredGroup.h or Monitored.h instead"
#include "AthenaMonitoring/MonitoredGroup.h"
#include "AthenaMonitoring/Monitored.h"   // TEMPORARY: for backwards compatibility

#endif
